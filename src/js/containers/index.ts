import { connect } from 'react-redux'
import Contacts from '../components/pages/Contacts'

export const ContactsContainer = connect((state:any) => ({ contact: state.contact.contact }))(Contacts)
