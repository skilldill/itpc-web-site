import * as React from 'react'
import { render } from 'react-dom'
import { HashRouter, Route, Switch } from 'react-router-dom'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'

import Home from './components/pages/Home'
import About from './components/pages/About'
import { ContactsContainer } from './containers'
import Menu from './components/Menu'
import NotFound from './components/pages/404'

import Reducers from './reducers'


const store = createStore(Reducers, applyMiddleware())

const test = (
    <Provider store={ store }>
        <ContactsContainer />
    </Provider>
)

render(
    <HashRouter>
        <div>
            <Menu />
            <Switch>
                <Route exact path="/" component={ Home } />
                <Route path="/about" component={ About } />
                <Route path="/contacts" component={ () => test }/>
                <Route component={ NotFound } />
            </Switch>
        </div>
    </HashRouter>,
    document.getElementById('itpc')
)
