import * as React from 'react'
import { Link } from 'react-router-dom'

export default class Menu extends React.Component<any, {}>{
    render(){
        return(
            <nav className="menu">
                <ul className="d-flex align-items-center">
                    <li className="p-2">
                        <Link to="/">
                            <span className="menu__item">главная</span>
                        </Link>
                    </li>
                    <li className="p-2">
                        <Link to="about">
                            <span className="menu__item">о компании</span>
                        </Link>
                    </li>
                    <li className="p-2">
                        <Link to="contacts">
                            <span className="menu__item">контакты</span>
                        </Link>
                    </li>
                    <li className="p-2">
                        <Link to="contacts">
                            <span className="menu__item">новости</span>
                        </Link>
                    </li>
                    <li className="ml-auto p-2">
                        <img className="logo" src="./static/img/logo/full_logo.png" alt="ТРИЦ" />
                    </li>
                </ul>
            </nav>
        )
    }
}