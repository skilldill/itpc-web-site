const gulp=require('gulp')
const webpack=require('webpack')
const webpackStream=require('webpack-stream')
const sass = require('gulp-sass')
const browser = require('browser-sync')
 
sass.compiler = require('node-sass')

gulp.task('js', ()=>{
    gulp.src('./src/js/**/*.tsx')
    .pipe(webpackStream({
        output:{
            filename: 'bundle.js'
        },
        resolve: {
          extensions: [".ts", ".tsx", ".js", ".json"]
        },
        module: {
          rules: [
              { test: /\.tsx?$/, loader: "awesome-typescript-loader" },
              { enforce: "pre", test: /\.js$/, loader: "source-map-loader" }
          ]
        },
        mode: 'production',
        externals: {}
    }), webpack)
    .pipe(gulp.dest('./app/static/js'))
})

gulp.task('sass', function () {
  return gulp.src('./src/css/style.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./app/static/css/'));
});

gulp.task('default', ['js', 'sass'])

// gulp.task('room-js', ()=>{
//     gulp.src('./src/js/app-room/**/*.tsx')
//     .pipe(webpackStream({
//         output:{
//             filename: 'room.js'
//         },
//         resolve: {
//           extensions: [".ts", ".tsx", ".js", ".json"]
//         },
//         module: {
//           rules: [
//               { test: /\.tsx?$/, loader: "awesome-typescript-loader" },
//               { enforce: "pre", test: /\.js$/, loader: "source-map-loader" }
//           ]
//         },
//         mode: 'production',
//         externals: {}
//     }), webpack)
//     .pipe(gulp.dest('./app/static/js'))
// })

// gulp.task('room', ['room-js', 'sass'])

// gulp.task('auth', ['auth-js', 'sass'])
  
// gulp.task('browser',()=>{
// 	browser({
// 		server:{
// 			baseDir:'app'
// 		}
// 	})
// })

// gulp.task('watch', ['browser', 'auth-sass'], ()=>{
//     gulp.watch('./app/index.html', browser.reload)//Для перезагрузки страницы при сохранении index.html
// })